#-------------------------------------------------
#
# Project created by QtCreator 2013-02-22T04:31:29
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = mvConsoleClient
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mvlinearcongruential.cpp \
    mvxsquare.cpp

HEADERS += \
    mvlinearcongruential.h \
    mvxsquare.h
