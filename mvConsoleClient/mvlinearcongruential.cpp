#include "mvlinearcongruential.h"
#include <time.h>
#include <fstream>
#include <math.h>

/* Default Constructor, to initialize a MvLinearCongruential object.
 * These values are based on those used in the book Numerical Recipes
 */
MvLinearCongruential::MvLinearCongruential()
{
    //this->X = time(NULL);
    //this->X = 12345;

    // Numerical Recipes
    this->a = 1664525;

    // MSVC
    //this->a = 214013;

    //this->c = 1013904223;

    //MSVC
    //this->c = 2531011;

    // Prime relative to m-prime
    this->c = 4200001021;

    //m-prime
    this->m = 4294967291;

    // Some things
    //this->a = 65;
    //this->c = 27;
    //this->m = 256;

    // VAX values
    //this->a = 269;
    //this->c = 245;
    //this->m = 4294967295;

    // TSV
    std::fstream file;
    unsigned int useed;
    file.open("/dev/random", std::fstream::out | std::fstream::binary);
    if (file.is_open())
    {
        file.read(reinterpret_cast<char*>(&useed), sizeof(useed));
        file.close();

        this->X = (useed xor time(NULL));
    }
    else
    {
        this->X = time(NULL);
        if (0 == (X % 2))
        {
            X++;
        }
    }
    //this->a = 1 + 4*(4);
    //this->c = 2147483647;
    //this->m = 2147483648; // 2^31
}

MvLinearCongruential::MvLinearCongruential(unsigned int seed)
{
    if (seed <= 0)
        X = time(NULL);
    else
        X = seed;

    a = 1664525;
    c = 1013904223;
    m = 4294967295;
}

/* This constructor initializes with the values supplied, only if they met the
 * little requeriments stated on mvlinearcongruential.h
 */
MvLinearCongruential::MvLinearCongruential(unsigned int seed, unsigned int modulus)
{
    if (seed <= 0)
        X = time(NULL);
    else
        X = seed;

    if (modulus <= X || modulus <= 0)
        m = 4294967295;
    else
        m = modulus;

    a = 1664525;
    c = 1013904223;
}

MvLinearCongruential::MvLinearCongruential(unsigned int seed, unsigned int multiplier, unsigned int additive)
{
    if (seed <= 0)
        X = time(NULL);
    else
        X = seed;

    if (multiplier <= 0)
        a = 1664525;
    else
        a = multiplier;

    if (additive <= 0)
        c = 1013904223;
    else
        c = additive;

    m = 4294967295;
}

MvLinearCongruential::MvLinearCongruential(unsigned int seed, unsigned int multiplier, unsigned int additive, unsigned int modulus)
{

    if (seed <= 0)
        X = time(NULL);
    else
        X = seed;

    if (multiplier <= 0)
        a = 1664525;
    else
        a = multiplier;

    if (additive <= 0)
        c = 1013904223;
    else
        c = additive;

    if (modulus <= X || modulus <= 0)
        m = 4294967295;
    else
        m = modulus;
}


unsigned int MvLinearCongruential::MvRand()
{
    X = (( a * X + c ) % m);
    //X = fmod((a * X + c), m);
    //X = ((X * a + c) & m) >> 16;
    return X;
}

float MvLinearCongruential::MvFRand()
{
    X = (( a * X + c ) % m);
    //F = fmod((a * F + c), m);
    F = ((float) X/(m));
    return F;
}
