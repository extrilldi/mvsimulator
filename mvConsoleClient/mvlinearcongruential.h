#ifndef MVLINEARCONGRUENTIAL_H
#define MVLINEARCONGRUENTIAL_H

class MvLinearCongruential
{
public:
    // These variables are the initial seed, multiplier, the additive const
    // and the modulus.
    unsigned int X;
    unsigned int a;
    unsigned int c;
    unsigned int m;
    float F;

    // Now, the different constructors
    // Default constructor
    MvLinearCongruential();
    MvLinearCongruential(unsigned int seed);
    MvLinearCongruential(unsigned int seed, unsigned int modulus);
    MvLinearCongruential(unsigned int seed, unsigned int multiplier, unsigned int additive);
    MvLinearCongruential(unsigned int seed, unsigned int multiplier, unsigned int additive, unsigned int modulus);

    unsigned int MvRand();
    float MvFRand();

};

#endif // MVLINEARCONGRUENTIAL_H
