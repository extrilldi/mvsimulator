#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <iostream>
#include "mvlinearcongruential.h"
#include "mvxsquare.h"


int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);
    int s = 1;
    int N = 0;
    int X = 0;
    int a = 0;
    int c = 0;
    int m = 0;

    std::cout<<"Generador de numeros pseudoaleatorios"<<std::endl;

    std::cout<<"Numeros pseudoaleatorios a generar: ";
    std::cin>>N;
    if (!std::cin)
    {
        std::cin.ignore();

        std::cout<<"Tome esto en serio, ingrese valores correctos. Terminando"<<std::endl;
        return -1;
    }

    std::cout<<"Usar los valores por defecto, o ingresar los propios?"<<std::endl;
    std::cout<<"1: default"<<std::endl;
    std::cout<<"2: ingresar"<<std::endl;

    std::cout<<"Seleccion: ";
    std::cin>>s;
    if (!std::cin)
    {
        std::cin.ignore();
        std::cout<<"Tome esto en serio, ingrese valores correctos. Terminando"<<std::endl;
        return -1;
    }

    if (s > 2 || s < 0)
    {
        std::cout<<"Tome esto en serio, ingrese valores correctos. Terminando"<<std::endl;
        return -1;
    }

    if (1 == s)
    {
        MvLinearCongruential* mk = new MvLinearCongruential();
        QFile f("numbers.txt");
        if (f.open(QFile::WriteOnly|QFile::Truncate|QFile::Text))
        {
            QTextStream str(&f);
            for (int i = 0; i < N; i++)
            {
                float k = ((float)mk->MvRand() / mk->m);
                str<<k<<endl;

            }
            f.close();

            std::cout<<"Numeros generados"<<std::endl;



            //return a.exec();

        }
        delete mk;

        MvXSquare* mxs = new MvXSquare(N);

        if (f.open(QFile::ReadOnly|QFile::Text))
        {
            QTextStream in(&f);
            while (!in.atEnd())
            {
                QString line = in.readLine();
                float number = line.toFloat();
                mxs->setFrequencyData(number);
            }

            f.close();

            std::cout<<"Se ha capturado la informacion"<<std::endl;
            std::cout<<"Calculando el estadistico de la prueba X^2"<<std::endl;
            float xSquareResult = mxs->calcXSquareValue();
            std::cout<<"El resultado es: "<<xSquareResult<<std::endl;
            std::cout<<"Compare con su tabla, con un grado de confianza de 95%"<<std::endl;
            std::cout<<"Y "<<(mxs->n - 1)<<" grados de libertad"<<std::endl;
        }

        delete mxs;

        return 0;
    }

    if (2 == s)
    {
        std::cout<<"Ingrese los valores siguentes:"<<std::endl<<std::endl;

        std::cout<<"Valor de la semilla inicial (default time(NULL)): ";
        std::cin>>X;
        if (!std::cin)
        {
            std::cin.ignore();
            std::cout<<"Tome esto en serio, ingrese valores correctos. Terminando"<<std::endl;
            return -1;
        }

        std::cout<<"Valor de la constante multiplicativa: ";
        std::cin>>a;
        if (!std::cin)
        {
            std::cin.ignore();
            std::cout<<"Tome esto en serio, ingrese valores correctos. Terminando"<<std::endl;
            return -1;
        }

        std::cout<<"Valor de la constante aditiva: ";
        std::cin>>c;
        if (!std::cin)
        {
            std::cin.ignore();
            std::cout<<"Tome esto en serio, ingrese valores correctos. Terminando"<<std::endl;
            return -1;
        }

        std::cout<<"Valor del modulo: ";
        std::cin>>m;
        if (!std::cin)
        {
            std::cin.ignore();
            std::cout<<"Tome esto en serio, ingrese valores correctos. Terminando"<<std::endl;
            return -1;
        }

        MvLinearCongruential* mk = new MvLinearCongruential(X,a,c,m);
        QFile f("numbers.txt");
        if (f.open(QFile::WriteOnly|QFile::Truncate|QFile::Text))
        {
            QTextStream str(&f);
            for (int i = 0; i < N; i++)
            {
                str<<mk->MvRand() * 1000<<endl;

            }
            f.close();

            std::cout<<"Numeros generados"<<std::endl;
            //return a.exec();
            return 0;
        }

    }

}
