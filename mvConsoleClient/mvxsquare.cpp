#include "mvxsquare.h"
#include <math.h>

MvXSquare::MvXSquare(int totalEvents)
{
    this->n = 5;
    this->N = totalEvents;
    initComponents();
}

MvXSquare::MvXSquare(int totalEvents, int intervals)
{
    this->n = intervals;
    this->N = totalEvents;
    initComponents();
}

void MvXSquare::initComponents()
{
    this->X = 0.0;
    this->expectedFreq = (this->N / this->n);

    float intervalSize = (1.0 / this->n);
    float mon = 0.000000001;
    struct mvXSquareTable t;
    for (int i = 0; i < this->n; i++)
    {
        t.intervalStart = mon;
        t.intervalEnd = mon + (intervalSize - 0.000000001);
        t.observedFreq = 0;
        this->table.push_back(t);
        mon += intervalSize;
    }
}

float MvXSquare::calcXSquareValue()
{
    for (int i = 0; i < this->n; i++)
    {
        float tmp1 = (this->expectedFreq - this->table[i].observedFreq);
        float tmp2 = pow(tmp1, 2);
        this->X += ((tmp2)/(this->expectedFreq));
    }
    return this->X;
}

void MvXSquare::setFrequencyData(float number)
{
    for (int i = 0; i < this->n; i++)
    {
        if ((number >= this->table[i].intervalStart) && (number <= this->table[i].intervalEnd))
        {
            this->table[i].observedFreq += 1;
        }
    }
}
