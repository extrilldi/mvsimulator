#ifndef MVXSQUARE_H
#define MVXSQUARE_H

#include <vector>

struct mvXSquareTable {
    float intervalStart;
    float intervalEnd;
    int observedFreq;
};

class MvXSquare
{
public:
    int N;
    int n;
    int expectedFreq;
    std::vector<mvXSquareTable> table;
    float X;

    MvXSquare(int totalEvents);
    MvXSquare(int totalEvents, int intervals);
    void initComponents();
    float calcXSquareValue();
    void setFrequencyData(float number);
};

#endif // MVXSQUARE_H
