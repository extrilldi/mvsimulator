#ifndef MVUNIFORMRANDVAR_H
#define MVUNIFORMRANDVAR_H

#include "MvUniformRandVar_global.h"

/*!
 * \class The MvUniformRandVar class
 *
 * This defines a Uniform variable generator object.
 */
class MVUNIFORMRANDVARSHARED_EXPORT MvUniformRandVar
{
private:
    unsigned int A; //!< The lower limit
    unsigned int B; //!< The upper limit

public:
    MvUniformRandVar(unsigned int bottomLimit, unsigned int upperLimit);
    double getUniformRandVar(double rand);
};

#endif // MVUNIFORMRANDVAR_H
