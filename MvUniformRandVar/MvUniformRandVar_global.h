#ifndef MVUNIFORMRANDVAR_GLOBAL_H
#define MVUNIFORMRANDVAR_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MVUNIFORMRANDVAR_LIBRARY)
#  define MVUNIFORMRANDVARSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MVUNIFORMRANDVARSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // MVUNIFORMRANDVAR_GLOBAL_H
