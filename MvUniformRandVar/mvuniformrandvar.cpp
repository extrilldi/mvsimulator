#include "mvuniformrandvar.h"

/*!
 * \brief MvUniformRandVar::MvUniformRandVar default and sole constructor
 * \param bottomLimit The lower limit
 * \param upperLimit The upper limit
 *
 * Sets A and B with the values passed. Simple.
 */
MvUniformRandVar::MvUniformRandVar(unsigned int bottomLimit, unsigned int upperLimit)
{
    this->A = bottomLimit;
    this->B = upperLimit;
}

/*!
 * \brief MvUniformRandVar::getUniformRandVar
 * \param rand A pseudo random number
 * \return The Uniform random variable
 *
 * This applies the algorithm as per the presentation 1.4b
 */
double MvUniformRandVar::getUniformRandVar(double rand)
{
    double X = A + ((B - A) * rand);
    return X;
}
