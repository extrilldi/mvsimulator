#-------------------------------------------------
#
# Project created by QtCreator 2013-03-05T08:20:23
#
#-------------------------------------------------

QT       -= gui

TARGET = MvUniformRandVar
TEMPLATE = lib

DEFINES += MVUNIFORMRANDVAR_LIBRARY

SOURCES += mvuniformrandvar.cpp

HEADERS += mvuniformrandvar.h\
        MvUniformRandVar_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
