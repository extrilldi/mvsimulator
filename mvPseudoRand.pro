TEMPLATE = subdirs

SUBDIRS += \
    mvGUIClient \
    MvLinearCongruential \
    MvPoissonRandVar \
    MvXSquare \
    MvExpoRandVar \
    MvUniformRandVar
