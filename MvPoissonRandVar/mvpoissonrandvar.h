#ifndef MVPOISSONRANDVAR_H
#define MVPOISSONRANDVAR_H

#include "MvPoissonRandVar_global.h"

/*!
 * \class The MvPoissonRandVar class
 *
 * This defines a Poisson random variable generator, using the algorithm made
 * by Donald Knuth
 */
class MVPOISSONRANDVARSHARED_EXPORT MvPoissonRandVar
{
public:
    double lambda; //!< The expected value of an expected event
    double p; //!< The mean
    static const double e = 2.718228; //!< MAGIC. One of those numbers
    double y; //!< A counter
    unsigned int x; //!< The variable to hold the random variable generated

    MvPoissonRandVar(double inLambda); //!< Default and only constructor

    //! Variable generation in iterative form
    bool generateVariable(double rand);
    //! Simple getter
    unsigned int getVariable();
    //! Set the variables at their instance creation values.
    void resetVars();
};

#endif // MVPOISSONRANDVAR_H
