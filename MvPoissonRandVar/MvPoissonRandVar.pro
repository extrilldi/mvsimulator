#-------------------------------------------------
#
# Project created by QtCreator 2013-02-26T08:54:36
#
#-------------------------------------------------

QT       -= gui

TARGET = MvPoissonRandVar
TEMPLATE = lib

DEFINES += MVPOISSONRANDVAR_LIBRARY

SOURCES += mvpoissonrandvar.cpp

HEADERS += mvpoissonrandvar.h\
        MvPoissonRandVar_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
