#include "mvpoissonrandvar.h"
#include <math.h>

// This is based on the assumption that lambda is
// defined for all the Random Variables

/*!
 * \brief MvPoissonRandVar::MvPoissonRandVar
 * \param inLambda Value of lambda to set
 *
 * This sets lambda as \a inLambda, calculates p as e^-lambda and sets the
 * counter y to 1, and the variable x to 0.
 */
MvPoissonRandVar::MvPoissonRandVar(double inLambda)
{
    this->lambda = inLambda;
    this->p = pow(this->e, ((this->lambda)*(-1)));
    this->y = 1.0;
    this->x = 0;
}

/*!
 * \brief MvPoissonRandVar::generateVariable
 * \param rand A pseudo random number
 * \return Boolean comprobation of variable generation
 *
 * This is intended to run in a loop. Such loop is not defined, and should be
 * made in the client executable. Until, as per the algorithm, y reaches a value
 * less than p, it returns false. Otherwise, x is then generated, and returns
 * true.
 */
bool MvPoissonRandVar::generateVariable(double rand)
{
    this->y *= rand;
    if (y < p)
        return true;
    else
    {
        x++;
        return false;
    }
}

/*!
 * \brief MvPoissonRandVar::getVariable
 * \return The Poisson Random Variable
 */
unsigned int MvPoissonRandVar::getVariable()
{
    return this->x;
}

/*!
 * \brief MvPoissonRandVar::resetVars
 *
 * Sets the counter to 1, and variable x to 0.
 */
void MvPoissonRandVar::resetVars()
{
    this->y = 1.0;
    this->x = 0;
}
