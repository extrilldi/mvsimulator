#ifndef MVLINEARCONGRUENTIAL_H
#define MVLINEARCONGRUENTIAL_H

#include "MvLinearCongruential_global.h"

/*!
 * \class The MvLinearCongruential class
 *
 * This defines a generator object using the Linear Congruential method, in mixed
 * mode.
 */
class MVLINEARCONGRUENTIALSHARED_EXPORT MvLinearCongruential
{
public:
    // The variables used to hold the data of the generator. Public for easy access
    // by other functions. It shouldn't, though
    unsigned int X; //!< Seed value
    unsigned int a; //!< Multiplicative constant
    unsigned int c; //!< Additive constant
    unsigned int m; //!< Modulo value; the period of the generator.
    float F;

    // Now, the different constructors
    MvLinearCongruential(); //!< Default constructor
    MvLinearCongruential(unsigned int seed);
    MvLinearCongruential(unsigned int seed, unsigned int modulus);
    MvLinearCongruential(unsigned int seed, unsigned int multiplier, unsigned int additive);
    MvLinearCongruential(unsigned int seed, unsigned int multiplier, unsigned int additive, unsigned int modulus);

    //! Generate the random number. As random as it can be with this.
    unsigned int MvRand();
    float MvFRand();

private:
    //! Private function to generate the initial seed X
    unsigned int getXi();
};

#endif // MVLINEARCONGRUENTIAL_H
