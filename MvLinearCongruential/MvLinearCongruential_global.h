#ifndef MVLINEARCONGRUENTIAL_GLOBAL_H
#define MVLINEARCONGRUENTIAL_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MVLINEARCONGRUENTIAL_LIBRARY)
#  define MVLINEARCONGRUENTIALSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MVLINEARCONGRUENTIALSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // MVLINEARCONGRUENTIAL_GLOBAL_H
