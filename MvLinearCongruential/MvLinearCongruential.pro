#-------------------------------------------------
#
# Project created by QtCreator 2013-02-26T08:03:37
#
#-------------------------------------------------

QT       -= gui

TARGET = MvLinearCongruential
TEMPLATE = lib

DEFINES += MVLINEARCONGRUENTIAL_LIBRARY

SOURCES += mvlinearcongruential.cpp

HEADERS += mvlinearcongruential.h\
        MvLinearCongruential_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
