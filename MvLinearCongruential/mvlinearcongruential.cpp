#include "mvlinearcongruential.h"
#include <time.h>
#include <fstream>
#include <math.h>

/*!
 * \brief MvLinearCongruential::MvLinearCongruential default constructor
 *
 * This uses default values to set the initial values of the generator.
 */
MvLinearCongruential::MvLinearCongruential()
{
    // Numerical Recipes
    this->a = 1664525;

    // Prime relative to m-prime, while c mod 200 = 21
    this->c = 4200001021;

    //m, a prime number as big as posible
    this->m = 4294967291;

    // Custom generated at run time
    this->X = getXi();
}

/*!
 * \brief MvLinearCongruential::MvLinearCongruential overloaded constructor
 * \param seed The initial seed
 *
 * This sets the initial seed with the values of \a seed. All other values are
 * left with default numbers. If \a seed <= 0, then the seed is set with the
 * default value too.
 */
MvLinearCongruential::MvLinearCongruential(unsigned int seed)
{
    if (seed <= 0)
        X = getXi();
    else
        X = seed;

    this->a = 1664525;
    this->c = 4200001021;
    this->m = 4294967291;
}

/*!
 * \brief MvLinearCongruential::MvLinearCongruential overloaded constructor
 * \param seed The initial seed
 * \param modulus The modulo, longest period of the generator
 *
 * This sets the initial seed with the values of \a seed, and the modulo m with
 * the values of \a modulus. All other values are left with default numbers.
 * If \a seed <= 0, then the seed is set with the default value.
 * If \a modulus <= 0, then the modulo is set with the default value.
 */
MvLinearCongruential::MvLinearCongruential(unsigned int seed,
                                           unsigned int modulus)
{
    if (seed <= 0)
        this->X = getXi();
    else
        this->X = seed;

    if (modulus <= X || modulus <= 0)
        this->m = 4294967291;
    else
        this->m = modulus;

    this->a = 1664525;
    this->c = 4200001021;
}

/*!
 * \brief MvLinearCongruential::MvLinearCongruential overloaded method
 * \param seed The initial seed
 * \param multiplier The multiplicative constant
 * \param additive The additive constant
 *
 * Same as previous constructors, sets the internal values as the parameters
 * supplied. If they are 0, uses the default ones.
 */
MvLinearCongruential::MvLinearCongruential(unsigned int seed,
                                           unsigned int multiplier,
                                           unsigned int additive)
{
    if (seed <= 0)
        this->X = getXi();
    else
        this->X = seed;

    if (multiplier <= 0)
        this->a = 1664525;
    else
        this->a = multiplier;

    if (additive <= 0)
        this->c = 4200001021;
    else
        this->c = additive;

    this->m = 4294967291;
}

MvLinearCongruential::MvLinearCongruential(unsigned int seed,
                                           unsigned int multiplier,
                                           unsigned int additive,
                                           unsigned int modulus)
{

    if (seed <= 0)
        this->X = getXi();
    else
        this->X = seed;

    if (multiplier <= 0)
        this->a = 1664525;
    else
        this->a = multiplier;

    if (additive <= 0)
        this->c = 4200001021;
    else
        this->c = additive;

    if (modulus <= X || modulus <= 0)
        this->m = 4294967291;
    else
        this->m = modulus;
}

/*!
 * \brief MvLinearCongruential::MvRand
 * \return The pseudo random number
 *
 * Applies the mixed linead congruential algorithm to get a random[sic] number.
 * As the values are stored and updated in the lifetime of the object, no other
 * storage is needed, at least at this level.
 */
unsigned int MvLinearCongruential::MvRand()
{
    X = (( a * X + c ) % m);
    return X;
}

/*!
 * \brief MvLinearCongruential::MvFRand
 * \return the pseudo random number in an interval between 0 and 1
 */
float MvLinearCongruential::MvFRand()
{
    X = (( a * X + c ) % m);
    //F = fmod((a * F + c), m);
    F = ((float)X/(m-1));
    return F;
}

/*!
 * \brief MvLinearCongruential::getXi
 * \return The value of the initial seed.
 *
 * This method opens the /dev/random file, to get a number made out of noise
 * in the computer running it. Then XORs it with the current time in UNIX timestamp
 * form and returns it.
 * If /dev/random cannot be open, returns the current UNIX timestamp. Adds 1 if
 * the timestamp is even.
 */
unsigned int MvLinearCongruential::getXi()
{
    std::fstream file;
    unsigned int useed;
    file.open("/dev/random", std::fstream::out | std::fstream::binary);
    if (file.is_open())
    {
        file.read(reinterpret_cast<char*>(&useed), sizeof(useed));
        file.close();
        return (useed xor time(NULL));
    }
    else
    {
        useed = time(NULL);
        if (0 == (useed % 2))
        {
            useed += 1;
        }
        return useed;
    }

}
