#ifndef MVEXPORANDVAR_H
#define MVEXPORANDVAR_H

#include "MvExpoRandVar_global.h"

/*! \class The MvExpoRandVar class
 *
 * This class is used to generate a single exponential random variable.
 * Has overloaded methods and constructors.
 */
class MVEXPORANDVARSHARED_EXPORT MvExpoRandVar
{
private:
    //! Private variable alpha
    /*!
     * This variable holds the mean time to be used to generate random values
     */
    double alpha;
public:
    MvExpoRandVar();
    MvExpoRandVar(double alpha);
    double getExpoRandVar(double rand);
    double getExpoRandVar(double alpha, double rand);
};

#endif // MVEXPORANDVAR_H
