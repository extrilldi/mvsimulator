#include "mvexporandvar.h"
#include <math.h>

/*!
 * \brief MvExpoRandVar::MvExpoRandVar default Constructor
 *
 * Creates an object, setting alpha to 0. It is supposed to be used with the
 * second getExpoRandVar method
 */
MvExpoRandVar::MvExpoRandVar()
{
    this->alpha = 0.0;
}

/*!
 * \brief MvExpoRandVar::MvExpoRandVar overloaded constructor
 * \param alpha The value of the mean time between arrivals
 *
 * This function sets the private variable alpha to \a alpha. To be used with
 * the first getExpoRandVar method.
 */
MvExpoRandVar::MvExpoRandVar(double alpha)
{
    this->alpha = alpha;
}

/*!
 * \brief MvExpoRandVar::getExpoRandVar first method
 * \param rand A pseudorandom number, made by another source/generator
 * \return The random variable
 *
 * First getExpoRandVar method. This function uses the formula found in
 * presentations to generate the variable: X = -a * log (R). Simple one.
 */
double MvExpoRandVar::getExpoRandVar(double rand)
{
    double X = ((alpha * -1) * log(rand));
    return X;
}

/*!
 * \brief MvExpoRandVar::getExpoRandVar second method
 * \param alpha The mean time between arrivals to considerate
 * \param rand A pseudorandom number, made by another source/generator
 * \return The random variable
 *
 * Overloaded method. This one simply accepts the mean time value \a alpha
 * besides the random number \a rand
 */
double MvExpoRandVar::getExpoRandVar(double alpha, double rand)
{
    this->alpha = alpha;
    double X = ((alpha * -1) * log(rand));
    return X;
}
