#-------------------------------------------------
#
# Project created by QtCreator 2013-03-04T08:55:37
#
#-------------------------------------------------

QT       -= gui

TARGET = MvExpoRandVar
TEMPLATE = lib

DEFINES += MVEXPORANDVAR_LIBRARY

SOURCES += mvexporandvar.cpp

HEADERS += mvexporandvar.h\
        MvExpoRandVar_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
