#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <vector>
#include <QVector>
#include <QStandardItemModel>

namespace Ui {
class MainWindow;
}

/*!
 * \brief The MvSimulationTable struct
 *
 * This struct holds the data to be used by a QStandardItemModel. A model to be
 * used by tha tableView widget.
 */
struct MvSimulationTable {
    unsigned int poissonVar;
    unsigned int poissonVarValue;
    unsigned int packet;
    double time;
    double accumulatedTime;
    unsigned int size;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;

    QString numbersFileName;
    QString testFileName;
    int numbersN;
    int testN;
    int testIntervals;
    int poissonLambda;
    int poissonN;
    std::vector<int> *poissonVariables;
    QVector<double> plotKeyData;
    QVector<double> plotValueData;
    std::vector<MvSimulationTable> *table;
    QStandardItemModel *simTableModel;

    bool writeNumbersToFile();
    bool writeCustomNumbersToFile();
    bool validateNumbersInput();
    bool getXSquareResults();
    bool validateTestInput();
    void setValuesTest();
    void showXSquare(double value);
    void tabulateData();
    void setPoissonPlot();
    void simulationShowTabbedData();

private slots:
    void on_btnNumbersDefault_clicked();
    void on_btnNumbersCustom_clicked();
    void on_btnNumFileSelect_clicked();
    void on_btnTestDo_clicked();
    void on_btnTestFileSelect_clicked();
    void on_btnPoissonVarGen_clicked();
    void on_btnGenerateTabData_clicked();
};

#endif // MAINWINDOW_H
