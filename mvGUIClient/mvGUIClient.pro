#-------------------------------------------------
#
# Project created by QtCreator 2013-02-25T23:53:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mvGUIClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    QCustomPlot/qcustomplot.cpp

HEADERS  += mainwindow.h \
    QCustomPlot/qcustomplot.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MvLinearCongruential/release/ -lMvLinearCongruential
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MvLinearCongruential/debug/ -lMvLinearCongruential
else:unix: LIBS += -L$$OUT_PWD/../MvLinearCongruential/ -lMvLinearCongruential

INCLUDEPATH += $$PWD/../MvLinearCongruential
DEPENDPATH += $$PWD/../MvLinearCongruential

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MvXSquare/release/ -lMvXSquare
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MvXSquare/debug/ -lMvXSquare
else:unix: LIBS += -L$$OUT_PWD/../MvXSquare/ -lMvXSquare

INCLUDEPATH += $$PWD/../MvXSquare
DEPENDPATH += $$PWD/../MvXSquare

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MvPoissonRandVar/release/ -lMvPoissonRandVar
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MvPoissonRandVar/debug/ -lMvPoissonRandVar
else:unix: LIBS += -L$$OUT_PWD/../MvPoissonRandVar/ -lMvPoissonRandVar

INCLUDEPATH += $$PWD/../MvPoissonRandVar
DEPENDPATH += $$PWD/../MvPoissonRandVar

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MvExpoRandVar/release/ -lMvExpoRandVar
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MvExpoRandVar/debug/ -lMvExpoRandVar
else:unix: LIBS += -L$$OUT_PWD/../MvExpoRandVar/ -lMvExpoRandVar

INCLUDEPATH += $$PWD/../MvExpoRandVar
DEPENDPATH += $$PWD/../MvExpoRandVar

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MvUniformRandVar/release/ -lMvUniformRandVar
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MvUniformRandVar/debug/ -lMvUniformRandVar
else:unix: LIBS += -L$$OUT_PWD/../MvUniformRandVar/ -lMvUniformRandVar

INCLUDEPATH += $$PWD/../MvUniformRandVar
DEPENDPATH += $$PWD/../MvUniformRandVar
