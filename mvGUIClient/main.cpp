#include "mainwindow.h"
#include <QApplication>

/*!
 * \brief main Main function loop in a Qt App
 * \param argc Console argument count
 * \param argv Console argument variable array
 * \return ISO STD return value
 *
 * This is just a wrapper function to a, the Qt Application entry point. It
 * effectively acts as a glue to the system std calls.
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
