#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>

#include "mvlinearcongruential.h"
#include "mvxsquare.h"
#include "mvpoissonrandvar.h"
#include "mvuniformrandvar.h"
#include "mvexporandvar.h"
#include "QCustomPlot/qcustomplot.h"

/*!
 * \brief MainWindow::MainWindow
 * \param parent the parent, in this case, the old main C++ method/entry point
 *
 * This sets up the whole window
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->input_N->setValidator(new QIntValidator(this));
    ui->inputTest_N->setValidator(new QIntValidator(this));
    numbersFileName = "";
    testFileName = "";

    // Set the spiners in the Simulation page
    // Why this number? Well, it is the maximum value an unsigned int can be.
    ui->inputDSB_X_Sim->setMaximum(4294967295);
    ui->inputDSB_X_Sim->setValue(0);
    ui->inputDSB_a_Sim->setMaximum(4294967295);
    ui->inputDSB_a_Sim->setValue(1664525);
    ui->inputDSB_c_Sim->setMaximum(4294967295);
    ui->inputDSB_c_Sim->setValue(4200001021);
    ui->inputDSB_m_Sim->setMaximum(4294967295);
    ui->inputDSB_m_Sim->setValue(4294967291);

    // Set the same spiner rules for the ones in the Number page.
    ui->input_DSB_X->setMaximum(4294967295);
    ui->input_DSB_X->setValue(0);
    ui->input_DSB_a->setMaximum(4294967295);
    ui->input_DSB_a->setValue(1664525);
    ui->input_DSB_c->setMaximum(4294967295);
    ui->input_DSB_c->setValue(4200001021);
    ui->input_DSB_m->setMaximum(4294967295);
    ui->input_DSB_m->setValue(4294967291);


    // Initialize the pointers to a NULL address, so it does not SEGFAULT when
    // trying to check if they exist.
    // TODO: Implement a garbage collector, or use a shared_ptr, or QSharedPointer
    poissonVariables = NULL;
    table = NULL;
    simTableModel = NULL;

}

/*!
 * \brief MainWindow::~MainWindow
 *
 * Normal destructor.
 */
MainWindow::~MainWindow()
{
    if (table)
    {
        delete table;
        table = NULL;
    }
    if (simTableModel)
    {
        delete simTableModel;
        simTableModel = NULL;
    }
    delete ui;
}

/*!
 * \brief MainWindow::on_btnNumFileSelect_clicked
 *
 * Function slot that triggers when the file selector is clicked. Opens a file in
 * the sense that it just get its name for future file operations in that file.
 */
void MainWindow::on_btnNumFileSelect_clicked()
{
    this->numbersFileName = QFileDialog::getOpenFileName(this,
                                                         tr("Abrir"),
                                                         QString(),
                                                         tr("Text Files (*.txt)"));
    this->ui->lineNumbersFileSel->setText(numbersFileName);
}

/*!
 * \brief MainWindow::on_btnTestFileSelect_clicked
 *
 * Same as on_btnNumFileSelect_clicked. It appears in the second tab of the form
 */
void MainWindow::on_btnTestFileSelect_clicked()
{
    this->testFileName = QFileDialog::getOpenFileName(this,
                                                      tr("Abrir"),
                                                      QString(),
                                                      tr("Text Files (*.txt)"));
    this->ui->lineTestFileSelect->setText(testFileName);
}

/*!
 * \brief MainWindow::on_btnNumbersDefault_clicked
 *
 * Function slot that triggers if you try to generate the pseudorandom numbers
 * First, it checks it there are no errors valitating data input. Then it tries
 * to write down the numbers to a file. Only if everything goes smooth, a good
 * message appears. Sets up the data input for the X Square test.
 */
void MainWindow::on_btnNumbersDefault_clicked()
{
    if(!validateNumbersInput())
    {
        QMessageBox::critical(this, tr("Errores"),
                              tr("No hay valorres especificados para N o el archivo de escritura"));
        return;
    }
    if(!writeNumbersToFile())
    {
        QMessageBox::critical(this, tr("Error"), tr("No se puede usar ese archivo"));
        return;
    }
    else
    {
        QMessageBox::information(this, tr("Numeros"),
                                 tr("El archivo seleccionado contiene los numeros generados"));
    }
    setValuesTest();

}

/*!
 * \brief MainWindow::on_btnNumbersCustom_clicked
 *
 * Function slot that triggers if you try to generate the pseudorandom numbers
 * First, it checks it there are no errors valitating data input. Then it tries
 * to write down the numbers to a file. Only if everything goes smooth, a good
 * message appears. Sets up the data input for the X Square test.
 * It just calls a different write function, that uses a different constructor.
 */
void MainWindow::on_btnNumbersCustom_clicked()
{
    if(!validateNumbersInput())
    {
        QMessageBox::critical(this, tr("Errores"),
                              tr("No hay valorres especificados para N o el archivo de escritura"));
        return;
    }
    if(!writeCustomNumbersToFile())
    {
        QMessageBox::critical(this, tr("Error"), tr("No se puede usar ese archivo"));
        return;
    }
    else
    {
        QMessageBox::information(this, tr("Numeros"),
                                 tr("El archivo seleccionado contiene los numeros generados"));
    }
    setValuesTest();

}

void MainWindow::on_btnTestDo_clicked()
{
    testFileName = this->ui->lineTestFileSelect->text();
    testIntervals = this->ui->inputSBIntervals->value();
    if(!validateTestInput())
    {
        QMessageBox::critical(this, tr("Errores"),
                              tr("No hay valores especificados para N o el archivo de escritura"));
        return;
    }
    if(!getXSquareResults())
    {
        QMessageBox::critical(this, tr("Error"), tr("Ocurrio un error inesperado"));
        return;
    }
    else
    {
        QMessageBox::information(this, tr("Test"), tr("Test hecho con exito"));
    }

}

void MainWindow::on_btnPoissonVarGen_clicked()
{
    // Fortunately, this does not need cute validations, because spinners
    poissonLambda = this->ui->inputSB_Lamda->value();
    poissonN = this->ui->inputSB_VarQuantity->value();

    // Point a new vector object, with the variable we had before.
    poissonVariables = new std::vector<int>();


    MvPoissonRandVar *mprv = new MvPoissonRandVar(poissonLambda);
    MvLinearCongruential *mk = new MvLinearCongruential();
    for (int i = 0; i < poissonN; i++)
    {
        for (;;)
        {
            float prand = ((float)mk->MvRand() / mk->m);
            if (mprv->generateVariable(prand))
                break;
        }
        poissonVariables->push_back(mprv->getVariable());
        mprv->resetVars();
    }
    // at this point, we should have all the random variables, so
    // let's destroy the objects
    delete mprv;
    mprv = NULL;
    delete mk;
    mk = NULL;

    // Now, the damn graph
    tabulateData();
    setPoissonPlot();

    // Now that we have plotted the variables, lets "clear the vector"
    // That just means that the pointer is clear for another use later.
    delete poissonVariables;
    poissonVariables = NULL;
}

void MainWindow::on_btnGenerateTabData_clicked()
{
    // Instantiate all the objects we'll be using. Their names make clear what
    // we are doing.
    MvLinearCongruential *mk = new MvLinearCongruential(
                this->ui->inputDSB_X_Sim->value(),
                this->ui->inputDSB_a_Sim->value(),
                this->ui->inputDSB_c_Sim->value(),
                this->ui->inputDSB_m_Sim->value());

    MvPoissonRandVar *mprv = new MvPoissonRandVar(
                this->ui->inputSB_LamdaSim->value());

    MvExpoRandVar *mxrv = new MvExpoRandVar(
                this->ui->inputDSB_alpha_Sim->value());

    MvUniformRandVar *murv = new MvUniformRandVar(
                this->ui->inputSB_limitBottom_Sim->value(),
                this->ui->inputSB_limitUp_Sim->value());

    // first, generate our Poisson Variables. This step is identical to the one
    // in the on_btnPoissonVarGen_clicked method up there. In fact, it should be
    // refactored
    // TODO: refactor this loop into a function
    int spvars = this->ui->inputSB_VarQuantitySim->value();
    poissonVariables = new std::vector<int>();
    for (int i = 0; i < spvars; i++)
    {
        for (;;)
        {
            float prand = ((float)mk->MvRand() / mk->m);
            if (mprv->generateVariable(prand))
                break;
        }
        poissonVariables->push_back(mprv->getVariable());
        mprv->resetVars();
    }

    // Let's test if table exists, just in case something went wrong. Not much
    // as existing, as pointing to an existing object.
    if (table)
    {
        delete table;
        table = NULL;
    }
    table = new std::vector<MvSimulationTable>();

    // Then a variable to accumulate time
    double aT = 0.0;

    // This loop first get the Poisson random variable, and using its value
    // rolls another loop, for the number of packets that it represents.
    // Then, in the inner loop, gets the values for its time and packet size,
    // to then pass it to a variable of the structure type, so it is pushed into
    // the array that hold the final table data (The one that is passed to the
    // ui->tableView

    for (unsigned int i = 0; i < poissonVariables->size(); i++)
    {
        unsigned int PoissonValue = poissonVariables->at(i);
        for (unsigned int k = 0; k < PoissonValue; k++)
        {
            double exp = mxrv->getExpoRandVar( ((float)mk->MvRand() / mk->m) );
            double uni = murv->getUniformRandVar( ((float)mk->MvRand() / mk->m) );
            struct MvSimulationTable t;
            t.poissonVar = (i + 1);
            t.poissonVarValue = PoissonValue;
            t.packet = (k + 1);

            // This little condition is for the very first 'packet' special case
            // As the time is 'randomly' generated, it should not matter, as it
            // does not affect posterior packets' times.

            if (0 == i && 0 == k)
            {
                t.time = 0.0;
            }
            else
            {
                t.time = exp;
            }

            aT += t.time;
            t.accumulatedTime = aT;

            t.size = (unsigned int) uni;
            table->push_back(t);
        }
    }

    // At this point, table shoud contain all the tabulated data.
    // So let's clear all the objects, except, of course, the table one.
    delete mk;
    delete mprv;
    delete mxrv;
    delete murv;
    mk = NULL;
    mprv = NULL;
    mxrv = NULL;
    murv = NULL;

    // Delete also the poissonVariable vector, as all the data is already
    // in table
    delete poissonVariables;
    poissonVariables = NULL;

    // Now, enable the button to show the data, and output a message
    // This should be done with a bool function
    // TODO: Refactor this in smaller pieces that return boolean codes.

    QMessageBox::information(this, tr("Simulacion"),
                             tr("Ahora puede visualizar la tabla de datos"));
    simulationShowTabbedData();
}

void MainWindow::simulationShowTabbedData()
{
    // First, we check if there is already data on memory
    // If exist, then we liberate that data, so we dont have leaks when reasigning
    // the pointer
    if (simTableModel)
    {
        delete simTableModel;
        simTableModel = NULL;
    }

    // Now, let's initialize the pointer, with a literally "new" object

    simTableModel = new QStandardItemModel(this);

    simTableModel->setHorizontalHeaderItem(0,
                                           new QStandardItem(
                                               QString("Tiempo entre arribo")));
    simTableModel->setHorizontalHeaderItem(1,
                                           new QStandardItem(
                                               QString("Tiempo acumulado")));
    simTableModel->setHorizontalHeaderItem(2,
                                           new QStandardItem(
                                               QString(
                                                   QString::fromUtf8("Tamaño de paquete"))));


    // In this case, the notation (*table)[i].var, does 3 things:
    // 1. It dereferences the object table, because it is a pointer.
    // 2. It gives us the data on location i.
    // 3. Because it is a structure, we get the value of 'var'.
    // To get this value, because we cant use the -> operator due to vector
    // having no way to output both data and index in the same sentence (as per
    // C++ 98), the 'old' way of doing it is used. That is, the (*). operator.

    for (unsigned int i = 0; i < table->size(); i++)
    {

        simTableModel->setItem(i, 0,
                               new QStandardItem(
                                   QString::number( (*table)[i].time )));
        simTableModel->setItem(i, 1,
                               new QStandardItem(
                                   QString::number( (*table)[i].accumulatedTime )));
        simTableModel->setItem(i, 2,
                               new QStandardItem(
                                   QString::number( (*table)[i].size )));
    }

    this->ui->tableView->setModel(simTableModel);

    // Now, a little retouch, because by default, the table is rendered quite
    // ugly
    this->ui->tableView->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    this->ui->tableView->resizeRowsToContents();

    // And now, delete the "table", because we have already graphicated it.
    delete table;
    table = NULL;
}

bool MainWindow::writeNumbersToFile()
{
    QFile file(numbersFileName);
    if (!file.open(QFile::WriteOnly | QFile::Truncate | QFile::Text))
    {
        return false;
    }
    else
    {
        QTextStream stream(&file);
        MvLinearCongruential *mk = new MvLinearCongruential();
        for (int i = 0; i < numbersN; i++)
        {
            float k = ((float)mk->MvRand() / mk->m);
            stream<<k<<endl;
        }
        file.close();
        delete mk;
        mk = NULL;

        return true;
    }
}

bool MainWindow::writeCustomNumbersToFile()
{
    QFile file(numbersFileName);
    if (!file.open(QFile::WriteOnly | QFile::Truncate | QFile::Text))
    {
        return false;
    }
    else
    {
        QTextStream stream(&file);
        MvLinearCongruential *mk = new MvLinearCongruential(
                    this->ui->input_DSB_X->value(),
                    this->ui->input_DSB_a->value(),
                    this->ui->input_DSB_c->value(),
                    this->ui->input_DSB_m->value());

        for (int i = 0; i < numbersN; i++)
        {
            float k = ((float)mk->MvRand() / mk->m);
            stream<<k<<endl;
        }
        file.close();
        delete mk;
        mk = NULL;

        return true;
    }
}

bool MainWindow::validateNumbersInput()
{
    if (this->ui->input_N->text().isEmpty())
    {
        return false;
    }
    else
    {
        bool ok;
        QString r = this->ui->input_N->text();
        this->numbersN = r.toInt(&ok,10);
        if (!ok)
        {
            return false;
        }
    }

    if (numbersFileName.isEmpty())
    {
        return false;

    }

    //supposedly, it shouldnt reach here even.
    return true;
}

void MainWindow::setValuesTest()
{
    this->testN = this->numbersN;
    this->testFileName = this->numbersFileName;

    this->ui->lineTestFileSelect->setText(testFileName);
    this->ui->inputTest_N->setText(QString::number(testN));
}

bool MainWindow::validateTestInput()
{
    // Technically, we dont need to validate the spinner
    // as it can put values between 1 or 100 inclusive.

    if (this->ui->inputTest_N->text().isEmpty())
    {
        return false;
    }
    else
    {
        bool ok;
        QString r = this->ui->inputTest_N->text();
        this->testN = r.toInt(&ok,10);
        if (!ok)
        {
            return false;
        }
    }
    if (testFileName.isEmpty())
    {
        return false;
    }

    return true;
}

bool MainWindow::getXSquareResults()
{
    QFile file(testFileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return false;
    }
    else
    {
        QTextStream stream(&file);
        MvXSquare *mxs = new MvXSquare(testN,testIntervals);
        for (int i = 0; i < testN; i++)
        {
            QString line = stream.readLine();
            float number = line.toFloat();
            mxs->setFrequencyData(number);
        }
        file.close();

        double XSquareResult = mxs->calcXSquareValue();
        showXSquare(XSquareResult);

        delete mxs;
        mxs = NULL;
        return true;
    }
}

void MainWindow::showXSquare(double value)
{
    QString msg = "El resultado de la prueba x^2 fue " + QString::number(value);
    msg.append("\n");
    msg.append("Consulte su tabla de la distribucion, con 95 % de confianza y ");
    msg.append(QString::number(this->testIntervals -1));
    msg.append(" grados de libertad");

    this->ui->label_TestResults->setText(msg);
}

// TODO: This thing should be a damn std::map, or something like that.
void MainWindow::tabulateData()
{
    int k = 0;
    for (int i = 0; i < poissonN; i++)
    {
        if (poissonVariables->at(i) > k)
        {
            k = poissonVariables->at(i);
        }
    }

    for (int i = 0; i < k; i++)
    {
        plotKeyData<<(i+1);
    }

    plotValueData.resize(plotKeyData.size());
    plotValueData.fill(0);

    for (int i = 0; i < poissonN; i++)
    {
        for (int r = 0; r < k; r++)
        {
            if (poissonVariables->at(i) == plotKeyData.at(r))
            {
                plotValueData.replace(r, (plotValueData.at(r)+1));
            }
        }
    }
}

void MainWindow::setPoissonPlot()
{
    QCPBars *myBars = new QCPBars(this->ui->graphPoissonPlot->xAxis,
                                  this->ui->graphPoissonPlot->yAxis);
    this->ui->graphPoissonPlot->addPlottable(myBars);
    // now we can modify properties of myBars:
    myBars->setName("Pretty bars");
    myBars->setData(plotKeyData, plotValueData);
    this->ui->graphPoissonPlot->rescaleAxes();
    this->ui->graphPoissonPlot->replot();

    // This function removes myBars, deleting it, so it doesnt leak.
    // It does not replots, so all is 'clear'.
    this->ui->graphPoissonPlot->clearPlottables();
}
