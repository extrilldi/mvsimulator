#-------------------------------------------------
#
# Project created by QtCreator 2013-02-26T10:04:46
#
#-------------------------------------------------

QT       -= gui

TARGET = MvXSquare
TEMPLATE = lib

DEFINES += MVXSQUARE_LIBRARY

SOURCES += mvxsquare.cpp

HEADERS += mvxsquare.h\
        MvXSquare_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
