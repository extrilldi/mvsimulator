#ifndef MVXSQUARE_H
#define MVXSQUARE_H

#include <vector>

#include "MvXSquare_global.h"

/*!
 * \brief The mvXSquareTable struct
 * Holds a row in a table consisting of the cells needed for a statistical
 * account of the Expected and Observed values per interval.
 */
struct mvXSquareTable {
    float intervalStart;
    float intervalEnd;
    int observedFreq;
};

/*!
 * \brief The MvXSquare class
 *
 * This defines a particular way of doing a X^2 test, holding data in memory.
 * This table hold in memory, uses the mvXSquareTable struct as a row, pushing
 * back multiple rows, per number of intervals, inside a std::vector 'table'
 */
class MVXSQUARESHARED_EXPORT MvXSquare
{
public:
    int N; //!< Total events
    int n; //!< Number of intervals
    int expectedFreq;
    std::vector<mvXSquareTable> table;
    float X; //!< Statistical value of the test

    MvXSquare(int totalEvents);
    MvXSquare(int totalEvents, int intervals);

    //! This function sets the initial values of each one of the 'rows' intervals
    void initComponents();
    float calcXSquareValue();
    void setFrequencyData(float number);
};

#endif // MVXSQUARE_H
