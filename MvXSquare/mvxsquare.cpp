#include "mvxsquare.h"
#include <math.h>

/*!
 * \brief MvXSquare::MvXSquare default constructor
 * \param totalEvents The number of events to tabulate
 *
 * This sets the number of intervals to 5 as default, N as \a totalEvents and
 * calls the initComponents function
 */
MvXSquare::MvXSquare(int totalEvents)
{
    this->n = 5;
    this->N = totalEvents;
    initComponents();
}

/*!
 * \brief MvXSquare::MvXSquare Overloaded constructor
 * \param totalEvents The number of events to tabulate
 * \param intervals The number of intervals to made in such tabulation (rows)
 *
 * This does the same as the default constructor, setting n to \a intervals
 */
MvXSquare::MvXSquare(int totalEvents, int intervals)
{
    this->n = intervals;
    this->N = totalEvents;
    initComponents();
}

/*!
 * \brief MvXSquare::initComponents
 *
 * This creates the table.
 * First, sets X, the statistical result, to 0.0
 * Sets the value of the expected frequency.
 * Sets the size of each interval, per a simple division of the 0-1 interval
 * Iterates, as a var i = 0 to the number of intervals, to set the lower and upper
 * limits in each interval. Saves all of this in a mvXSquareTable t temporal
 * variable, that is then pushed back to the std::vector<mvXSquareTable> table.
 */
void MvXSquare::initComponents()
{
    this->X = 0.0;
    this->expectedFreq = (this->N / this->n);

    float intervalSize = (1.0 / this->n);
    float mon = 0.000000001;
    struct mvXSquareTable t;
    for (int i = 0; i < this->n; i++)
    {
        t.intervalStart = mon;
        t.intervalEnd = mon + (intervalSize - 0.000000001);
        t.observedFreq = 0;
        this->table.push_back(t);
        mon += intervalSize;
    }
}

/*!
 * \brief MvXSquare::calcXSquareValue
 * \return The statistical result
 *
 * This function should be run after the data has been captured, by running the
 * setFrequencyData function in a loop, for example.
 * Calculates, per interval, the diference between the expected frequency and the
 * observed one, squares it, and acummulates it in X (The summatory) after
 * dividing it by the expected frequency.
 */
float MvXSquare::calcXSquareValue()
{
    for (int i = 0; i < this->n; i++)
    {
        float tmp1 = (this->expectedFreq - this->table[i].observedFreq);
        float tmp2 = pow(tmp1, 2);
        this->X += ((tmp2)/(this->expectedFreq));
    }
    return this->X;
}

/*!
 * \brief MvXSquare::setFrequencyData
 * \param number The event to tabulate in a cell
 *
 * This compares \a number to a cell in table, so set it as an 'observed' frequency
 */
void MvXSquare::setFrequencyData(float number)
{
    for (int i = 0; i < this->n; i++)
    {
        if ((number >= this->table[i].intervalStart) && (number <= this->table[i].intervalEnd))
        {
            this->table[i].observedFreq += 1;
        }
    }
}
